<?php

// Template Name: How it's works template

get_header();
global $smof_data;
?>

<style>
	.post-type .avada-row {
		margin-bottom: 30px;
		padding-bottom: 30px;
		border-bottom: 1px solid #ccc;
		position: relative;
			padding-left: 30px;
		padding-right: 30px;
	}
	#wrapper .post-content .post-type h2{color:#747474}
	.arrow-bot {
		position: absolute;
		width: 42px;
		height: 42px;
		background: url(http://thanksforsupporting.com/wp-content/uploads/2017/10/arrow-1.png);
		display: block;
		left: 50%;
		bottom: -20px;
		    z-index: 9;
}
	}
	
	.post-type h2 {
		color: #777777
	}
	
	.post-type .img {
		text-align: center;
	}
	.post-type  .link-btn {
    display: block;
    width: 200px;
    margin: 0 auto; text-align: center;
	}
	.page-template-template-how-its-works #main{padding-left: 0;
padding-right: 0;}
	.page-template-template-how-its-works #main .avada-row{ max-width: 100%;}
</style>

<div class="post-content">

	<div class="fusion-fullwidth fullwidth-box post-type">



		<div class="avada-row" style="background: #f8f8f8; padding-top: 30px;">
			<div class="col-sm-7 col-lg-7 col-md-7">
<div class="row">
				<div class="col-sm-2 col-lg-2 col-md-2"><span class="post-no">1</span>
				</div>
				<div class="col-sm-10 col-lg-10 col-md-10">
					<h2>

						<?php 

$my_id2 = 11051;

$post_id_72 = get_post($my_id2); 

echo $title2 = $post_id_72->post_title;

?>
					</h2>

					<?php 

$my_id2 = 11051;

$post_id_72 = get_post($my_id2); 

echo $cont = $post_id_72->post_content;

?>

					

				</div>

</div>
<a class="link-btn blue-btn" href="<?php the_field('button_link',11051); ?>">
						<?php the_field('button_name',11051); ?>
					</a>
			</div>

			<div class="col-sm-5 col-lg-5 col-md-5 img">

				<?php

				$my_id2 = 11051;

				$post_id_72 = get_post( $my_id2 );

				$title2 = $post_id_72->post_title;

				echo get_the_post_thumbnail( $my_id2, 'large' );

				?>



			</div>



			<span class="arrow-bot"></span>

		</div>



		<div class="avada-row" style=" margin-bottom: 0;">
		<div class="col-sm-7 col-lg-7 col-md-7 pull-right">
<div class="row">
				<div class="col-sm-2 col-lg-2 col-md-2"> <span class="post-no">2</span>
				</div>

				<div class="col-sm-10 col-lg-10 col-md-10">
					<h2>

						<?php 

$my_id2 = 11053;

$post_id_72 = get_post($my_id2); 

echo $title2 = $post_id_72->post_title;

?>
					</h2>

					<?php 

$my_id2 = 11053;

$post_id_72 = get_post($my_id2); 

echo $cont = $post_id_72->post_content;

?>









				</div>

</div>

				
			</div>
			<div class="col-sm-5 col-lg-5 col-md-5 img">

				<?php

				$my_id2 = 11053;

				$post_id_72 = get_post( $my_id2 );

				$title2 = $post_id_72->post_title;

				echo get_the_post_thumbnail( $my_id2, 'large' );

				?>



			</div>
			
			<span class="arrow-bot"></span>
		</div>

		<div class="avada-row" style="background: #f8f8f8; padding-top: 30px; border-bottom: none;">
			<div class="col-sm-7 col-lg-7 col-md-7">
				<div class="row"><div class="col-sm-2 col-lg-2 col-md-2"><span class="post-no">3</span>
				</div>
				<div class="col-sm-10 col-lg-10 col-md-10">
					<h2>

						<?php 

$my_id2 = 11055;

$post_id_72 = get_post($my_id2); 

echo $title2 = $post_id_72->post_title;

?>
					</h2>

					<?php 

$my_id2 = 11055;

$post_id_72 = get_post($my_id2); 

echo $cont = $post_id_72->post_content;

?>

				</div>
			</div>
			<a class="link-btn" href="<?php the_field('button_link',11055); ?>">
						<?php the_field('button_name',11055); ?>
					</a>
			</div>

			<div class="col-sm-5 col-lg-5 col-md-5 img">

				<?php

				$my_id2 = 11055;

				$post_id_72 = get_post( $my_id2 );

				$title2 = $post_id_72->post_title;

				echo get_the_post_thumbnail( $my_id2, 'large' );

				?>



			</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>