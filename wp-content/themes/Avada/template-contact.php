<?php

// Template Name: Contact template

get_header(); global $smof_data; ?>



	<div id="content" style=" width: 100%;">

		<?php while(have_posts()): the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php echo avada_render_rich_snippets_for_pages(); ?>

			<?php echo avada_featured_images_for_pages(); ?>	

			<div class="post-content">

				<div class="avada-row">  <div class="col-sm-6 col-lg-6 col-md-6"><?php the_content(); ?> 

			  <a class="link-btn blue-btn" href="#"  data-toggle="modal" data-target="#myModal">Contact Form</a>

			  </div>

				  <div class="col-sm-6 col-lg-6 col-md-6"><?php dynamic_sidebar('avada-custom-sidebar-contactmap'); ?></div>

			</div>	</div></div>

		<?php endwhile; ?>

	</div>

	

<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h2 class="modal-title">Contact Us</h2>

      </div>

      <div class="modal-body">

       <?php echo do_shortcode('[contact-form-7 id="11018" title="Contact form 1"]'); ?>

      </div>



    </div>



  </div>

</div>

<?php get_footer(); ?>