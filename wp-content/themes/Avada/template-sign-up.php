<?php

// Template Name:  Sign Up template

get_header(); 
global $smof_data; 
?>

	<div id="content" style=" width: 100%;">

		<?php while(have_posts()): the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php echo avada_render_rich_snippets_for_pages(); ?>

			<?php echo avada_featured_images_for_pages(); ?>	
			<div class="post-content">
				<?php the_content(); ?>
				<?php avada_link_pages(); ?>
			</div>
			<div class="post-content a">

				<div class="avada-row"> 
					<div class="new-account"><?php dynamic_sidebar('avada-custom-sidebar-signup');?></div>
				  </div>

			</div>	</div>

		<?php endwhile; ?>

	</div>
	
<?php get_footer(); ?>